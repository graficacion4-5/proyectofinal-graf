#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <OpenGL/glu.h>
# include <GLUT/glut.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>
#endif
GLboolean  command = FALSE;
int alterna = 0;//variable usada como bandera para borrar o pintar latortga en 3d
int ejes = 0;//variable usada como bandera para borrar o pintar los ejes
GLfloat traslacion = 28.0 ; //tranlacion de los objetos laterales
char strCommand[256]; 


/*se define la siguiente variable  "mModel" como global, la cual guardar� de modo independiente
la matriz de modelado debido  a que en  OpenGL, la matriz de modelado se almacena junto
con la de visualizaci�n en la matriz GL_MODELVIEW y necesitamos dicha matriz para calcular las
cordenadas de la tortuga*/
GLdouble mModel[16];




//********* M�todo para pintar los ejes***********************
void dibujaEjes(void){
	glBegin(GL_LINES);
	glColor3f(0.0,0.0,1.0);//eje azul Z
	glVertex3f(0.0,0.0,-9.0);
	glVertex3d(0.0,0.0,9.0);
	
	glColor3f(0.0,1.0,0.0);
	glVertex3f(-9.0,0.0,0.0);//eje verde X
	glVertex3d(9.0,0.0,0.0);
	
	glColor3f(1.0,0.0,0.0);//eje rojo Y
	glVertex3f(0.0,-9.0,0.0);
	glVertex3d(0.0,9.0,0.0);
	glEnd();
}
	//*********fin del M�todo para pintar los ejes de la tortuga***********************
	
	
	/* Codigo de los colores en RGB
	R   G   B   result
	1.0 0.0 0.0 red, duh
	0.0 1.0 0.0 green, duh
	0.0 0.0 1.0 blue, duh
	1.0 1.0 0.0 yellow
	1.0 0.0 1.0 purple
	0.0 1.0 1.0 cyan
	1.0 1.0 1.0 white
	1.0 0.5 0.0 orange
	0.5 1.0 0.0 greenish yellow..
	0.5 1.0 0.5 light green
	0.0 0.5 0.0 dark green
	
	
	*/
	
	
	
	//************M�todo para recibir instrucciones por medio del teclado **********
	
	void keyboard(int key, int x, int y) { 
		
		glPushMatrix();
		glLoadIdentity(); 
		glMultMatrixd(mModel);//esta instrucci�n multiplica la matriz superior de la pila por la matriz que tiene como argumento.
		//dejando en la posici�n superior de la pila a la matriz mModel.
		switch (key) {
			
		case GLUT_KEY_LEFT :     
		{
			
			glTranslatef(-0.2,0.0 , 0.0);
		}
		break ; //desplazamiento en -x
		case GLUT_KEY_RIGHT :    
		{
			
			glTranslatef(0.2,0.0 , 0.0);
			
		}
		break ; //desplazamiento en +x
		
		case 'e':                          //Pinta o borra ejes
			if (ejes == 1) {ejes = 0;}
			else {ejes++;}
			break;
			
		case 'i':
			command = TRUE;
			break;
		case 'q':
		case 27:
			exit(0); 
			break;
		}
		glutPostRedisplay(); 
		
		//Al finalizar el bloque de opciones se recupera la matriz mModel y se restaura la que estaba previamente en la pila con el c�digo: 
		glGetDoublev (GL_MODELVIEW_MATRIX, mModel); 
		glPopMatrix();
		
		
	}
	
	
	void drawCarretera()
	{
		
		glPushMatrix();
		glRotatef(90.0,1.0,0.0,0.);
		glTranslatef(0,-24.5,0);
		glScalef(2,21,0.0005);
		glColor3f(0.5,0.5,0.5);
		glutSolidCube(2.5);
		glPopMatrix();
		
		
	}
	
	void drawLaterales()
	{
		
		glPushMatrix();
		glTranslatef(0,-0.09,0);
		glScalef(5,1.8,1);
		glColor3f(1.0,0.5,0.0);
		glutSolidCube(1.3);
		glPopMatrix();
		
		
	}
	void drawCielo()
	{
		
		glPushMatrix();
		glTranslatef(0,1.5,-2.5);
		glScalef(5,1,1);
		glColor3f(0.0,0.0,1.0);
		glutSolidCube(1.3);
		glPopMatrix();
		
		
		
	}
	drawAnimall(float x){
		glPushMatrix();
		glTranslatef(-2.5,0,-traslacion+x);
		//cuerpo
		glPushMatrix();
		glTranslatef(-2.5,0,2);
		glScalef(1,0.5,0.25);
		glColor3f(1.0,1.0,1.0);
		glutSolidCube(1);
		glPopMatrix();
	
		//Manchas
		glPushMatrix();
		glTranslatef(-2.5,0,2);
		glScalef(0.1,0.1,0.2);
		glColor3f(0.0,0.0,0.0);
		glutSolidCube(1);
		glPopMatrix();
		
		glPushMatrix();
		glTranslatef(-2.8,0.1,2);
		glScalef(0.1,0.1,0.2);
		glColor3f(0.0,0.0,0.0);
		glutSolidCube(1);
		glPopMatrix();
		
		glPushMatrix();
		glTranslatef(-2.2,0.1,2);
		glScalef(0.1,0.1,0.2);
		glColor3f(0.0,0.0,0.0);
		glutSolidCube(1);
		glPopMatrix();
		
		//cabeza
		glPushMatrix();
		glTranslatef(-1.8,0.2,2);
		glScalef(0.5,0.5,0.2);
		glColor3f(1.0,1.0,1.0);
		glutSolidCube(1);
		glPopMatrix();
		
		//patas
		glPushMatrix();
		glTranslatef(-4.8,0.2,2);
		glScalef(0.1,0.4,0.2);
		glColor3f(1.0,1.0,2.0);
		glutSolidCube(1);
		glPopMatrix();
		
		glPopMatrix();
	}
		
		drawAnimal(float x){
			glPushMatrix();
			glTranslatef(-2.5,0,-traslacion+x);
			//edificio
			glPushMatrix();
			glTranslatef(5,1.5,5);
			glRotatef(0,1.0,0.0,0.);
			glScalef(0.5,1,0.5);
			glColor3f(0.0,1.0,0.0);
			glutSolidCube(3);
			glPopMatrix();
				
			
			//ventanas
			glPushMatrix();
			glTranslatef(4.3,0.5,5);
			glScalef(0,0.5,1.5);
			glColor3f(1.0,1.0,1.0);
			glutSolidCube(1);
			glPopMatrix();

			glPushMatrix();
			glTranslatef(4.3,1.5,5);
			glScalef(0,0.5,1.5);
			glColor3f(1.0,1.0,1.0);
			glutSolidCube(1);
			glPopMatrix();
			
			glPushMatrix();
			glTranslatef(4.3,2.5,5);
			glScalef(0,0.5,1.5);
			glColor3f(1.0,1.0,1.0);
			glutSolidCube(1);
			glPopMatrix();
			
			
			glPopMatrix();
		}
	drawArbol(float x){
			glPushMatrix();
			glTranslatef(-2.5,0,-traslacion+x);
			//tronco
			glPushMatrix();
			glTranslatef(-2.5,0,2);
			glScalef(0.25,1.8,0.25);
			glColor3f(0.698,0.3411,0.007);
			glutSolidCube(1);
			glPopMatrix();
			
			//circulo
			glPushMatrix();
			glTranslatef(-2.8,1.5,0.5);
			glScalef(1.5,1.19,0.25);
			glColor3f(0.0,0.5,0.0);
			glutSolidSphere(1,16,16);
			glPopMatrix();
			
			//manzanas
			glPushMatrix();
			glTranslatef(-2.8,2,0.5);
			glScalef(1.5,1.19,0.25);
			glColor3f(1.0, 0.0, 0.0);
			glutSolidSphere(0.1,16,16);
			glPopMatrix();
			
			glPushMatrix();
			glTranslatef(-2.8,0.9,0.5);
			glScalef(1.5,1.19,0.25);
			glColor3f(1.0, 0.0, 0.0);
			glutSolidSphere(0.1,16,16);
			glPopMatrix();
			
			glPushMatrix();
			glTranslatef(-3.8,1.4,0.5);
			glScalef(1.5,1.19,0.25);
			glColor3f(1.0, 0.0, 0.0);
			glutSolidSphere(0.1,16,16);
			glPopMatrix();
			
			glPushMatrix();
			glTranslatef(-1.8,1.4,0.5);
			glScalef(1.5,1.19,0.25);
			glColor3f(1.0, 0.0, 0.0);
			glutSolidSphere(0.1,16,16);
			glPopMatrix();
			
			
			
			
			
			glPopMatrix();
		}
	
	
	// ************  M�todo para dibujar la tortuga en 3d
	void drawMoto(GLfloat r,GLfloat g, GLfloat b){
		//eje rojo es x
		
		glPushMatrix();
		glScalef(0.5,0.5,0.5);
		glRotatef(180,0,90,0);
		glTranslatef(0.0,0.0,-2.0);
		//	Salpicadera Delantera
		glPushMatrix();
		glRotatef(5.0,5.0,0.0,0.);
		glScalef(0.5,0.2,1);
		glTranslatef(0.0, 1.45,0.3);
		glColor3f(r,g,b);
		glutSolidCube(0.2);
		glPopMatrix();
		
		//Salpicadera Trasera
		glPushMatrix();
		glRotatef(-65.0,1.0,0.0,0.);
		glScalef(0.5,0.2,1);
		glTranslatef(0.1,-0.43,0.2);
		glColor3f(r,g,b);
		glutSolidCube(0.2);
		glPopMatrix();
		
		//	Tijera Delantera
		glPushMatrix();
		glRotatef(65.0,1.0,0.0,0.);
		glScalef(1.0,0.2,2.4);
		glTranslatef(0.0, 1.7,-0.025);
		glColor3f(0.7,0.7,0.7);
		glutSolidCube(0.2);
		glPopMatrix();
		
		//	Tijera Trasera
		glPushMatrix();
		glRotatef(-65.0,1.0,0.0,0.);
		glScalef(1.0,0.2,1.7);
		glTranslatef(0.0, 2.0,-0.025);
		glColor3f(0.7,0.7,0.7);
		glutSolidCube(0.2);
		glPopMatrix();
		
		//Motor
		glPushMatrix();
		glTranslatef(0.0, 0.14,-0.03);
		glScalef(1.0,0.6,1.3);
		glColor3f(0.5,0.5,0.5);
		glutSolidCube(0.2);
		glPopMatrix(); 
		
		//CarcasaMotor
		glPushMatrix();
		glTranslatef(0.0, 0.26,-0.03);
		glScalef(1.0,0.6,2.3);
		glColor3f(r,g,b);
		glutSolidCube(0.2);
		glPopMatrix(); 
		
		
		
		//Estrella
		glPushMatrix();
		glColor3f(0.5,0.5,0.5); 
		glTranslatef(0.0, 0.0, 0.4);
		glRotatef(90.0,0.0,180.0,0.);
		glutSolidSphere(0.05,20,20);
		glPopMatrix();
		
		//Estrella
		glPushMatrix();
		glColor3f(0.5,0.5,0.5); 
		glTranslatef(0.0, 0.0, -0.4);
		glRotatef(90.0,0.0,180.0,0.);
		glutSolidSphere(0.05,20,20);
		glPopMatrix();
		
		
		
		//Rin delantero
		glPushMatrix();
		glColor3f(0.7,0.7,0.7); 
		glTranslatef(0.0, 0.0, 0.4);
		glRotatef(90.0,0.0,180.0,0.);
		glutSolidTorus(0.02,0.08,15,15);
		glPopMatrix();
		
		
		//Rin trasero
		glPushMatrix();
		glColor3f(0.7,0.7,0.7); 
		glTranslatef(0.0, 0.0, -0.4);
		glRotatef(90.0,0.0,180.0,0.);
		glutSolidTorus(0.02,0.08,15,15);
		glPopMatrix();
		
		//llanta delantera
		glPushMatrix();
		glColor3f(0.0,0.0,0.0); 
		glTranslatef(0.0, 0.0, 0.4);
		glRotatef(90.0,0.0,180.0,0.);
		glutSolidTorus(0.07,0.16,15,15);
		glPopMatrix();
		
		//llanta trasera
		glPushMatrix();
		glColor3f(0.0,0.0,0.0); 
		glTranslatef(0.0, 0.0, -0.4);
		glRotatef(90.0,0.0,180.0,0.);
		glutSolidTorus(0.07,0.16,15,15);
		glPopMatrix();
		
		
		//Cuerpo
		glPushMatrix();
		glTranslatef(0, 0.3,-0.1);
		glScalef(1.0,0.2,3.2);
		glColor3f(r,g,b);
		glutSolidCube(0.2);
		glPopMatrix(); 
		
		
		//Manubrio
		glPushMatrix();
		glRotatef(90,0,90,0);
		glScalef(0.3,0.2,2.5);
		glTranslatef(-0.54, 2.15,0.0);
		glColor3f(0.7,0.7,0.7);
		glutSolidCube(0.2);
		glPopMatrix();
		
		//asiento
		glPushMatrix();
		glTranslatef(0.0, 0.30,-0.03);
		glScalef(1.0,0.3,1.3);
		glColor3f(0,0,0);
		glutSolidCube(0.2);
		glPopMatrix(); 
		
		
		
		glPopMatrix();
		
	}
		void drawMotosCHoque(GLfloat r,GLfloat g, GLfloat b)
		{
			
			glPushMatrix();
			glRotatef(180,0,90,0);
			glTranslatef(0.0,0.0,3.0);
			//glPushMatrix();
			drawMoto(r,g,b);
			// glPopMatrix();
			glPopMatrix();
			
		}
		
		// ************  Fin del M�todo para dibujar la tortuga en 3d				
		void display(void) {//definida como funci�n callback para dibujar o redubujarla ventana cada vez que sea necesario
			//Todas	las funciones de OpenGL comienzan con el prefijo � gl� y en muchas (como es el caso de glColor3f) aparece un sufijo compuesto por un n�mero y una letra. El n�mero
			//simboliza el numero de par�metros que se debe pasar a la funci�n y la letra, el tipo de 	estos par�metros
			
						
			glClearColor(1.0, 1.0, 1.0, 0.0);//establece el color de fono de la ventana, que es con el que se �borra� la ventana.
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//e borrar el fondo de la ventana.Acepta como argumento el buffer espec�fico que se desea borrar, en este caso el
			drawLaterales();
			drawCarretera();
			drawCielo();
			drawMotosCHoque(1.0,0.0,0.0);
			glPushMatrix();
			glMultMatrixd(mModel); 
			glColor3f(1.0,1.0,0.0);
			
			if(ejes == 0){//si no hay ejes se dibujan
				glPushMatrix();
				dibujaEjes();
				glPopMatrix();} 
			
			glPushMatrix();			
			drawMoto(0.0,0.0,1.0);
			glPopMatrix();
			
			glPushMatrix();
			drawArbol(0.0);
			glPopMatrix();
			
			glPushMatrix();
			drawAnimal(14.0);
			glPopMatrix();
			
			
			glPopMatrix(); 
			glutSwapBuffers();// se encarga de intercambiar el buffer posterior con el buffer anterior y es necesaria porque se ha definido que se trabaja con doble buffer.
		}                          //Cuando se dibuja cualquier figura, esta es dibujada en el buffer posterior (el que est�  atr�s) y cuando el dibujo est� terminado los dos buffers se intercambian.
		
		
		
		//metodo para ver escenas
		void reshape(int width, int height) {//es del tipo void. Se le pasan como argumentos el ancho y el alto de la ventana despu�s del reescalado.
			//glViewport(0, 0, width, height);//define la porci�n de ventana donde OpenGL podr� dibujar
			//1er Parametro distancia horizontal y vertical de la esquina superior 	izquierda del � cuadro� donde OpenGL puede dibujar con respecto a la ventana;
			//2d9 parametro, el ancho y alto de la ventana.
			
			//***Modificaci�n de glViewport para que no se deforme
			if(width<=height)
				glViewport(0, 0, width, width);
			else
				glViewport(0, 0, height, height);
			
			
			glMatrixMode(GL_PROJECTION);//especifica la matriz de transformaci�n sobre la que se van a realizar las operaciones siguientes 
			//Los 3 tipos d matrices que existen: de  proyecci�n (GL_PROJECTION), de modelado (GL_MODELVIEW) y  de textura (GL_TEXTURE)
			//glMatrixMode(GL_PROJECTION) afecta la perspectiva de la	proyecci�n
			
			glLoadIdentity();//carga como matriz de proyecci�n la matriz 	identidad.
			
			//gluPerspective(60.0, (GLfloat)height / (GLfloat)width, 1.0, 128.0); //opera sobre la matriz de proyecci�n y define el �ngulo del campo de visi�n en sentido
			//vertical (en grados), la relaci�n entre la altura y la anchura de la figura (aspecto), el
			//plano m�s cercano a la c�mara y el plano m�s lejano de la c�mara, respectivamente.
			/* Estos dos �ltimos son los planos de corte, que son los que se encargan de acotar el
			volumen de visualizaci�n por delante y por detr�s de la figura. Todo lo que est� por
			delante del plano m�s cercano y todo lo que est� detr�s del plano m�s lejano no ser� representado en la ventana.*/
			
			gluPerspective(60.0, 1.0, 1.0, 128.0);
			
			
			glMatrixMode(GL_MODELVIEW);//define que las operaciones que se 	realicen a continuaci�n afectar�n a la matriz de modelado
			glLoadIdentity();//Nuevamente se carga la matriz identidad
			gluLookAt(0.0, 1.0, 3.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);// define la transformaci�n sobre la vista inicial. 
			//tiene 9 par�metros: los primeros tres representan la distancia en x, y, z de los ojos del observador;
			// los siguientes tres, las coordenadas x,y, z del punto de referencia a observar y
			// los �ltimos tres, la direcci�n del upVector.
		}
		
		
		int main(int argc, char** argv) {
			glutInit(&argc, argv);//es la funci�n que inicializa la librer�a GLUT y negocia con el sistema de ventanas la apertura de una nueva ventana.
			//sus parametros son los mismos q de la funcion main
			glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);//define el modo en el que se debe dibujar la ventana.
			//GLUT_RGB indica el tipo de modelo de color con el que se dibujar� (Red-Green-Blue), GLUT_DEPTH indica que se debe incluir un
			// buffer de profundidad y GLUT_DOUBLE que se debe utilizar un doble buffer.			
			//Antes de crear una ventana, es necesario definir sus propiedades. Con la funci�n			glutInitWindowSize(749, 800);// define el tama�o de la ventana en p�xeles (anchura y altura
			glutInitWindowPosition(25, 20);//la distancia horizontal y vertical con respecto dla esquina superior izq del monitor donde la ventana deber�	aparecer.
			glutCreateWindow("CarrerasPeligrosas");//se crea la	ventana ccon nombre asignado x el string de argumento
			
			glMatrixMode(GL_MODELVIEW); 
			glPushMatrix(); 
			glLoadIdentity();
			glGetDoublev (GL_MODELVIEW_MATRIX, mModel); 
			glPopMatrix();
			
			glutSpecialFunc(keyboard);
			
			glutDisplayFunc(display);//define que la funci�n display que es pasada como argumento sea ejecutada cada vez que GLUT determine
			//que la ventana debe ser dibujada (la primera vez que se muestra la ventana) o redibujada (cuando se maximiza, cuando se superponen varias ventanas, etc).
			
			glutReshapeFunc(reshape);//Esta funci�n callback especifica cu�l funci�n ser� llamada cada vez que la ventana sea redimensionada o movida, pero tambi�n es
			//utilizada para definir inicialmente el �rea de proyecci�n de la figura en la ventana
			
			glutMainLoop();//pasar el control del flujo del programa a la GLUT, de manera que cada 	vez que ocurra un �evento� sean llamadas las funciones definidas como callbacks
			//hasta que el la ventana se cierre.
			return 0;
		}
		
		
		
